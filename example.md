## GitLab
Everyone Can Contribute

<img data-src="img/logo-square.png" height="300" class="plain">

Note:
Here's my speaker note


+++

## Demo 2
<!-- .slide: data-background-color="#ff0000" -->
Slide 1.2

---

## Demo 2
Slide 2

---

## Test code slide

<pre>
<code class="javascript">
if( typeof define === 'function' && define.amd ) {
		// AMD. Register as an anonymous module.
		define( function() {
			root.Reveal = factory();
			return root.Reveal;
		} );
	} else if( typeof exports === 'object' ) {
		// Node. Does not work with strict CommonJS.
		module.exports = factory();
	} else {
		// Browser globals.
		root.Reveal = factory();
	}
</code></pre>